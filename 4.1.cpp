﻿#include <iostream>
#include <fstream>

struct stek
{
    int value;
    struct stek* next;                      // указатель на следующий элемент списка (стека)
};

void push(stek*& NEXT, const int VALUE)
{
    stek* MyStack = new stek;               // объявляем новую динамическую переменную типа stek
    MyStack->value = VALUE;                 // записываем значение, которое помещается в стек
    MyStack->next = NEXT;                   // связываем новый элемент стека с предыдущим
    NEXT = MyStack;                         // новый элемент стека становится его вершиной
}

int pop(stek*& NEXT, std::ofstream& out)
{
    int temp = NEXT->value;                 // извлекаем в переменную temp значение в вершине стека
    stek* MyStack = NEXT;                   // запоминаем указатель на вершину стека, чтобы затем
                                            // освободить выделенную под него память
    NEXT = NEXT->next;                      // вершиной становится предшествующий top элемент
    delete MyStack;                         // освобождаем память, тем самым удалили вершину
    out << temp << "\n";   

    return temp;                            // возвращаем значение, которое было в вершине
}

int main()
{
    std::ifstream in("input.txt");
    std::ofstream out("output.txt");
    int n;
    char c;
    stek* p = 0;
    in >> n;
    for (int i = 0; i < n; i++)
    {

        in >> c;
        int s;
        if (c == '+') {
            in >> s;
            push(p, s);

        }
        else {

            pop(p, out);
        }
    }

    return 0;
}