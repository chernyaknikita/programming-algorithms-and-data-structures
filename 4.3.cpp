﻿#include <fstream>

using namespace std;

struct node {
    node* next;
    char key;

    node(char _key, node* _next) {
        key = _key;
        next = _next;
    }
};

int main() {
    ifstream fin("input.txt");
    ofstream fout("output.txt");
    unsigned numberOfStrings;

    fin >> numberOfStrings;
    node* stackTop;

    for (unsigned i = 0; i < numberOfStrings; i++) {
        string sequence;
        bool isLegal = true;
        stackTop = nullptr;
        fin >> sequence;

        for (unsigned j = 0; j < sequence.size(); j++) {
            if (sequence[j] == '(' || sequence[j] == '[') {
                stackTop = new node(sequence[j], stackTop);
            }
            else if ((stackTop)
                && ((sequence[j] == ')' && stackTop->key == '(')
                    || (sequence[j] == ']' && stackTop->key == '['))) {
                node* temp = stackTop;
                stackTop = stackTop->next;
                delete temp;
            }
            else {
                isLegal = false;
                break;
            }
        }
        if (isLegal && !stackTop) {
            fout << "YES" << endl;
        }
        else {
            fout << "NO" << endl;
        }
    }
}
