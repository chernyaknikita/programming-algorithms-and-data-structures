//#include "pch.h"
#include <iostream>
#include <fstream>


void quickSort(int arr[], int left, int right, int k1, int k2) {

	int i = left, j = right;

	int tmp;


	int pivot = arr[(left + right) / 2];

	/* partition */

	while (i <= j) {

		while (arr[i] < pivot)

			i++;

		while (arr[j] > pivot)

			j--;

		if (i <= j) {

			tmp = arr[i];

			arr[i] = arr[j];

			arr[j] = tmp;

			i++;

			j--;

		}

	}



	/* recursion */
	if ((k1 < j) && (k2 < j)) {

		if (left < j) {
			quickSort(arr, left, j, k1, k2);
		}
	}
	else if ((k1 > i) && (k2 > i)) {
		if (i < right)

			quickSort(arr, i, right, k1, k2);
	}
	else {
		if (left < j) {
			quickSort(arr, left, j, k1, k2);
		}
		if (i < right)

			quickSort(arr, i, right, k1, k2);

	}


}




int main()
{
	std::ifstream fin("input.txt");
	std::ofstream fout("output.txt");

	int n;
	fin >> n;
	int k1;
	fin >> k1;
	int k2;
	fin >> k2;
	int a;
	fin >> a;
	int b;
	fin >> b;
	int c;
	fin >> c;
	int *array = new int[n];
	fin >> array[0];
	fin >> array[1];

	for (int i = 2; i < n; ++i) {
		array[i] = a * array[i - 2] + b * array[i - 1] + c;
	}


	quickSort(array, 0, n - 1, k1, k2);

	for (int i = k1 - 1; i < k2; i++)
		fout << array[i] << ' ';
	fout << '\n';

	delete[] array;

	return 0;
}