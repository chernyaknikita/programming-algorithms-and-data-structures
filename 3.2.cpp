//
//  main.cpp
//  pads
//
//  Created by Alexey Ageev.
//  Copyright © 2020 Alexey Ageev. All rights reserved.
//
//MARK: Debug
//#include <ctime>
//unsigned long startTime = clock();
//#include <iostream>
//#include <fstream>

//MARK: Code
#include "edx-io.hpp"

int main(){
    int number;
    int length;
    int phases;
    
    io >> number >> length >> phases;
    
    int *indexes = new int[number];
    std::string *strings = new std::string[length];
    for (int i = 0; i < number; i++){
        indexes[i] = i;
    }
    for (int i = 0; i < length; i++){
        io >> strings[i];
    }
    for (int i = 0; i < phases; i++){
        unsigned letters[26] = { 0 };
        int *newIndex = new int[number];
        
        for (int j = 0; j < number; j++){
            letters[strings[length - i - 1][j] - 'a']++;
        }
        
        for (char j = 0; j < 25; j++){
            letters[j + 1] += letters[j];
        }
        for (char j = 25; j > 0; j--){
            letters[j] = letters[j - 1];
        }
        letters[0] = 0;

        for (int j = 0; j < number; j++){
            newIndex[letters[strings[length - i - 1][indexes[j]]- 'a']++] = indexes[j];
        }
        delete[] indexes;
        indexes = newIndex;
    }
    
    for (int i = 0; i < number; i++){
        io << indexes[i] + 1 << ' ';
    }
//    std::cout << std::endl << "!!!COMMENT DEBUG LINES BEFORE TESTING!!!" << std::endl << "Work time: " << 1000 * (clock() - startTime) / CLOCKS_PER_SEC << std::endl;
}
