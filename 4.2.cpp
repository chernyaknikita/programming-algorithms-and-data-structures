﻿#include <iostream>
#include <fstream>
#include <queue>  // подключили библиотеку queue

using namespace std;

int main() {
    queue <int> q;  // создали очередь q
    std::ifstream in("input.txt");
    std::ofstream out("output.txt");
    int n;
    char c;
    in >> n;
    for (int i = 0; i < n; i++)
    {

        in >> c;
        int s;
        if (c == '+') {
            in >> s;
            q.push(s);

        }
        else {
            out << q.front() << endl;
            q.pop();
        }
    }

    
    return 0;
}