main = do
    file <- readFile "input.txt"
    writeFile "output.txt" (show . rpc $ words (lines file !! 1))

rpc :: [String] -> Integer
rpc = head . foldl stacking []
    where   stacking (a : b : stackTail) "+" = (a + b) : stackTail
            stacking (a : b : stackTail) "-" = (b - a) : stackTail
            stacking (a : b : stackTail) "*" = (a * b) : stackTail
            stacking stack numberString = read numberString : stack
