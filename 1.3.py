from functools import reduce
def conc(x, y):
    return x + ' ' + str(y)
file = open('input.txt').read().split('\n')
data = list(map(int, file[1].split(' ')))
log = []
for i in range(1, int(file[0])):
    j = i - 1
    while j >= 0 and data[j] > data[j + 1]:
        data[j], data[j + 1] = data[j + 1], data[j]
        j -= 1
    log.append(j + 2)
open('output.txt', 'a').write(reduce(conc, log, '1') + '\n' + reduce(conc, data[1:], str(data[0])))